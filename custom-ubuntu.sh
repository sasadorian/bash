#! /bin/bash
#
# Author:       Sevak Asadorian
# Description:  Installation/customization script
# Date created: 20130713
# Tested on:    Ubuntu 13.04
# Description:  install/config
#               -customize terminal profile
#               -add custom aliases
#               -emacs (customized)
#               -gvim (customized)
#               -chrome
#               -netflix
#               -spotify
#               -eclipse (java, c++, g++)
#               -remove unity lens shopping
#               -meld diff viewer
#               -sublime text
#               -xbmc
#               -update/upgrade all

# for apt-add-repository
export FORCE_ADD_APT_REPOSITORY=force

# install banner
sudo apt-get install -y sysvbanner
wait

#
# configure terminal profile
#
banner profile
echo "customizing terminal profile" >> ./status.txt
# set the use of theme colors to FALSE
gconftool-2 --set "/apps/gnome-terminal/profiles/Default/use_theme_colors" --type bool false
# set background color to BLACK
gconftool-2 --set "/apps/gnome-terminal/profiles/Default/background_color" --type string "#000000"
# set foreground color to GREEN
gconftool-2 --set "/apps/gnome-terminal/profiles/Default/foreground_color" --type string "#00FF00"

#
# add custom aliases
#
banner aliases
echo "adding custom aliases" >> ./status.txt
# WARNING: append to file! (do not overwrite contents of .bashrc with '>' operator)
# NOTE: replaces previousley set aliases
echo "
# adding my aliases
alias b='cd ..; pwd;'
alias c='clear'
alias h='history 100'
alias l='ls -alrt'
alias mine='chmod og-rwx'
alias safe='chmod a-w'
e() { emacs \"\$1\" & }
f() { find . -name \"\$1\" -exec grep -in \"\$2\" {} /dev/null \; ; }
ff() { find . -name \"\$1\" ;}" >> ~/.bashrc

#
# install emacs
#
banner emacs
echo "installing emacs" >> ./status.txt 
sudo apt-get install -y emacs24
wait
# configure emacs
echo ";; disable menu bar
(menu-bar-mode -1)
;; diable tool bar
(tool-bar-mode -1)
;; dark theme
(invert-face 'default)
;; remove startup screen
(custom-set-variables
 '(inhibit-startup-screen t))
(custom-set-faces)" >> ~/.emacs

#
# install gvim
#
banner gvim
echo "installing gvim" >> ./status.txt
sudo apt-get install -y vim-gnome
wait

# configure gvim
echo "\"\"\" set my color scheme
colorscheme darkblue

\"\"\" turn on syntax highlighting
syntax on

\"\"\" indentation
\" [Tab] will be 3 spaces
set tabstop=3
\" use space instead of tab character
set expandtab
\" indent 3 spaces
set shiftwidth=3
\" columns to use when you hit Tab in 'insert mode'
set softtabstop=3
\" turn on auto indent
set autoindent
\" does the right thing (mostly)
set smartindent

\"\"\" hide training wheels
\" hide menu bar
set guioptions-=m
\" hide toolbar
set guioptions-=T
\" hide right hand scroll bar
set guioptions-=r
\" hide left hand scroll bar
set guioptions-=L

\"\"\" misc
\" switch on search pattern highlighting
set hlsearch
\" dont wrap lines
set nowrap
\" display line numbers
set number

if has(\"gui_running\")
   if has(\"gui_gtk2\")
      set guifont=Monospace\\ 8
   endif
endif" >> ~/.gvimrc

#
# install git
#
banner git
echo "installing git" >> ./status.txt
sudo apt-get install -y git
wait

#
# install chrome (64 bit)
#
banner chrome
echo "installing chrome" >> ./status.txt
cd /tmp
# download latest stable build
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
wait
# install the package
sudo dpkg -i google-chrome-stable_current_amd64.deb
wait
# recommended in case there was an error in previous step
sudo apt-get -f install -y
wait

#
# install netflix
#
banner netflix
echo "installing netflix" >> ./status.txt
sudo sh -c "echo ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true | debconf-set-selections"
#sudo apt-get install -y ttf-mscorefonts-installer --quiet
wait
sudo apt-add-repository -y ppa:ehoover/compholio
wait
sudo apt-get update
wait
sudo apt-get install -y netflix-desktop
wait

#
# install spotify
#
banner spotify
echo "installing spotify" >> ./status.txt
sudo add-apt-repository "deb http://repository.spotify.com stable non-free"
wait
sudo apt-get update -y
wait
sudo apt-get install --force-yes -y spotify-client-qt
wait

#
# install eclipse (java, c++, g++)
#
banner eclipse
echo "installing eclipse" >> ./status.txt
# silent license agreement
echo debconf shared/accepted-oracle-license-v1-1 select true | sudo debconf-set-selections
echo debconf shared/accepted-oracle-license-v1-1 seen true | sudo debconf-set-selections
# install java
sudo apt-get install -y openjdk-7-jdk
wait
sudo apt-get install -y oracle-java8-installer
wait
# install eclipse
sudo apt-get install -y eclipse eclipse-jdt eclipse-cdt g++
wait

#
# remove unity lens shopping (amazon)
#
sudo apt-get remove -y unity-lens-shopping

#
# meld
#
banner meld
echo "installing meld" >> ./status.txt
sudo apt-get install -y meld
wait

#
# install sublime text editor
#
banner sublime
echo "installing sublime text 3" >> ./status.txt
sudo add-apt-repository -y ppa:webupd8team/sublime-text-3
wait
sudo apt-get -y update
wait
sudo apt-get install -y sublime-text-installer
wait

#
# install xbmc
#
banner xbmc
echo "installing xbmc" >> ./status.txt
sudo add-apt-repository ppa:team-xbmc/ppa -y
wait
sudo apt-get update && sudo apt-get install xbmc -y
wait

#
# update/upgrade everything
#
banner update
echo "updating and upgrading" >> ./status.txt
sudo apt-get -y update
wait
sudo apt-get update -y && sudo apt-get upgrade -y
wait

#
# end of script
#
echo
echo
echo "End of script."
